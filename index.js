addEventListener('load',init);

function init() {
	addEventListener('click',function () {
		var x = event.clientX;     // Get the horizontal coordinate
		var y = event.clientY;
		console.log('('+x+','+y+')');
	});
	
	// global vars
	var bcanvas, bctx, vid, canvas, ctx, cw, ch, ops=[];
	var colorpick = document.querySelector('#colorpick');
	color = [255,255,255];
	
	createVid(); // create video - display hidden, paused and waiting to be used
	initCanvas(); // initialize the canvases so they are ready for the user
	setTimeout(function(){vid.play();},1000);
	processFrame(); // animation loop

/************* FIRE BORDER FUNCTIONALITY ******************/
	

function createVid() {
	var vidwrap = document.createElement('div');
	vidwrap.id='vidwrapper';
	document.querySelector('body').appendChild(vidwrap);
	vid = document.createElement('video');
	var vidsrc = document.createElement('source');
	vid.id='videoinv';
	vidsrc.src='demo-vid.mp4';
	vidsrc.setAttribute("type","video/mp4");
	vid.setAttribute('autoplay','autoplay');
	vid.setAttribute('loop','loop');
	vid.setAttribute('preload','auto');
	vid.style.display='none';
	vid.playbackRate=0.3;
	vid.setAttribute('crossOrigin', '');
	vid.appendChild(vidsrc);
	document.querySelector('#vidwrapper').appendChild(vid);
}

function initCanvas() {
	//initialize the canvas to be ready
	canvas = document.createElement('canvas'); //video output canvas
	ctx = canvas.getContext('2d');
	canvas.id='vidcanvas';
	bcanvas = document.createElement('canvas'); // buffer canvas
	bctx = bcanvas.getContext('2d');
	bcanvas.id='bvidcanvas';
	document.querySelector('#myvid').appendChild(canvas);
	
	cw=300;
	ch=150;
	canvas.style.width=  cw+'px';
	canvas.style.height= ch+'px';
	bcanvas.style.width= cw+'px';
	bcanvas.style.height=ch+'px';
	
	canvas.style.width=  cw;
	canvas.style.height= ch;
	bcanvas.style.width= cw;
	bcanvas.style.height=ch;
	
}


function processFrame() {
	// This is the main animation loop
	// process the video image data on a buffer canvas
	//console.log(color)
	bctx.drawImage(vid,0,0,cw,ch);
	var image = bctx.getImageData(0,0,cw,ch);
	var imageData = image.data;
	for (let i=3;i<imageData.length;i+=4) {
	//	// The video that they gave us has alpha data, but they all have a value of 255, meaning fully visable
	//	// I created my own transparent effect by removing the black background
	//	// set the alpha value to the average of rgb 0-255
	//	let op1 = Math.abs(imageData[i-3] - color[0]);
	//	let op2 = Math.abs(imageData[i-2] - color[1]);
	//	let op3 = Math.abs(imageData[i-1] - color[2]);
		let op = calcD([imageData[i-3],imageData[i-2],imageData[i-1]],color);
		//let op = 255;//( op1+op2+op3 ) / 3;
		imageData[i]=op;
	//	ops.push(op);
	}
	//draw the new image data on the canvas
	ctx.putImageData(image,0,0,0,0,cw,ch);
	animID = (window.requestAnimationFrame(processFrame));
	
}
//setTimeout(function(){console.log(ops);},3000);

function calcD(p1,p2) {
	var d = Math.hypot(p1[0]-p2[0],p1[1]-p2[1],p1[2]-p2[2])/Math.sqrt(3);
	return d
}

}

